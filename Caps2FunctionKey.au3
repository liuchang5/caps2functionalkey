;
; AutoIt Version: 3.0
; Language:       English
; Platform:       Win9x/NT
; Author:         liuchang
;
; Script Function:
;   Change my caps lock to function key

;==========================
; Includes
;==========================
#include <Constants.au3>
#include <Misc.au3>
#include <Debug.au3>

;==========================
; Globale variables
;==========================
Global $hUser32DLL      = DllOpen("user32.dll")
Global $bDebugMode      = True
Global $bSelectionMode  = False
Global $bSetHotKey		= False

;==========================
; Constain value
;==========================
Global Const $KEY_CAPS_LOCK  = "14"
Global Const $KEY_SHIFT_KEY  = "10"
Global Const $KEY_SPACEBAR   = "20"
Global Const $KEY_LEFT_SHIFT = "A0"
Global Const $KEY_ALT = "12"

;==========================
; Key maps to another key
;==========================
Local $MAX_KEY_MAP_COUNT = 100
Local $KeyMapArray[$MAX_KEY_MAP_COUNT][2] = [ _
	["i", "{UP}"], _
	["k", "{DOWN}"], _
	["j", "{LEFT}"], _
	["l", "{RIGHT}"], _
	["o", "^{RIGHT}"], _
	["u", "^{LEFT}"], _
	["h", "{HOME}"], _
	[";", "{END}"], _
	["n", "{PGUP}"], _
	["m", "{PGDN}"], _
	["f", "{BACKSPACE}"], _
	["d", "{DELETE}"], _
	["s", "{HOME}{END}{DELETE}"], _
	["g", "{ENTER}"], _
	["{CAPSLOCK}", ""], _
	["_EndTag_", ""] _
]

;==========================
; Key maps to a function
;==========================
Local $MAX_KEY_FUNC_COUNT = 100
Local $KeyFunArray[$MAX_KEY_FUNC_COUNT ][2] = [ _
	["CAPSLOCK", ""], _
	[" ", ""], _ ;space
	["_EndTag_", ""] _
]

Func UPs()
	_DebugOut("Upds")
EndFunc

; remap functions
Func _MyFunctionKey()
	Local $PressedKey = @HotKeyPressed

	; Key func array
	For $i = 0 To $MAX_KEY_MAP_COUNT - 1 Step 1
		If $PressedKey == $KeyFunArray[$i][0] Then
			Call($KeyFunArray[$i][1])
			;_DebugOut("Key func:" & $KeyFunArray[$i][0] & $KeyFunArray[$i][1])
			ExitLoop
		EndIf

		If  $KeyFunArray[$i][0] == "_EndTag_" Then
			ExitLoop
		EndIf
	Next

	If $bSelectionMode Then
		Send("{SHIFTDOWN}")
	EndIf


	; Key map array
	For $i = 0 To $MAX_KEY_MAP_COUNT - 1 Step 1
		If $PressedKey == $KeyMapArray[$i][0] Then
			Send($KeyMapArray[$i][1])
			;_DebugOut("Key maping:" & $KeyMapArray[$i][0] & "->" & $KeyMapArray[$i][1])
			ExitLoop
		EndIf

		If  $KeyMapArray[$i][0] == "_EndTag_" Then
			ExitLoop
		EndIf
	Next

	; Release the shif button
	If $bSelectionMode Then
		Send("{SHIFTUP}")
	EndIf

EndFunc


;==========================
; Hook functions
;==========================

Func _SetHotKey()
	If $bSetHotKey == True Then
		Return
	EndIf

	For $i = 0 To $MAX_KEY_MAP_COUNT - 1 Step 1
		HotKeySet($KeyMapArray[$i][0], "_MyFunctionKey")
		HotKeySet($KeyFunArray[$i][0], "_MyFunctionKey")
	Next

	$bSetHotKey = True

EndFunc

Func _UnsetHotKey()
	If $bSetHotKey == False Then
		Return
	EndIf

	For $i = 0 To $MAX_KEY_MAP_COUNT - 1 Step 1
		HotKeySet($KeyMapArray[$i][0])
		HotKeySet($KeyFunArray[$i][0])
	Next

	$bSetHotKey = False
EndFunc

;==========================
; Main programe
;==========================

If $bDebugMode Then
	_DebugSetup("DebugWindow", True)
	_DebugOut  ("Debug mode start~")
EndIf

; Specifies if AutoIt should store the state of
; capslock before a Send() function and restore it afterwards.
; 0 = don't store/restore
; 1 = (default) store and restore
AutoItSetOption("SendCapslockMode", 0)

; Alters the length of the brief pause in between sent keystrokes.
; A value of 0 removes the delay completely.
AutoItSetOption("SendKeyDelay", 0)

AutoItSetOption("SendKeyDownDelay", 0)

While 1
	Sleep(1)

	$bPressedCaps   = _IsPressed($KEY_CAPS_LOCK, $hUser32DLL) ;14 CAPS LOCK key
	If $bPressedCaps  Then
		$bSelectionMode  = _IsPressed($KEY_SPACEBAR , $hUser32DLL)
	EndIf

	If $bPressedCaps Then
		_SetHotKey()
	Else
		_UnsetHotKey()
		; allways turn off
		Send("{CAPSLOCK off}");
	EndIf
WEnd

DllClose($hUser32DLL)